import sys
sys.path.append('/usr/local/python')
from openpose import pyopenpose as op
from ..openpose_settings import openpose_settings

from libs.model import *
from libs.dataset import rp_normalization
import torch
import numpy
import random
from libs.seed import seed

random.seed(seed)
torch.manual_seed(seed)


class Analyzer:
    def __init__(self, model=None):
        self.openpose = op.WrapperPython()
        self.openpose.configure(openpose_settings)
        self.openpose.start()
        self.model_checkpoint = model
        checkpoint = torch.load(self.model_checkpoint)
        self.detector = FallDetector(state_dict=checkpoint["net"])
        self.detector.to("cuda:0")
        self.fall_count = 0

    def analyze(self, mat):
        datum = op.Datum()
        datum.cvInputData = mat
        self.openpose.emplaceAndPop(op.VectorDatum([datum]))
        if datum.poseKeypoints is not None:
            pose = numpy.delete(datum.poseKeypoints[0], 2, 1)
            pose = pose.reshape(-1, 1)
            pose = pose[:30]
            pose = rp_normalization(mat.shape[1], mat.shape[0], pose)
            pose_tensor = torch.Tensor(pose).to("cuda:0")
            label = self.detector.predict(pose_tensor)
            if label == 0:
                if self.fall_count > 0:
                    self.fall_count -= 1
            else:
                self.fall_count += 1
                if self.fall_count >= 3:
                    self.fall_count = 0
                    return 1, datum.cvOutputData

        return 0, datum.cvOutputData
