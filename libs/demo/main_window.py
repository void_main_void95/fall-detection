import gi
import numpy as np

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GdkPixbuf, Gdk
from .gstreamer_worker import *
from .gsreamer_renderer import *
from .analyzer import *
import datetime

__SPACING__ = 10


class MainWindow(Gtk.Window):
    def __init__(self, config, model):
        super().__init__(title="FallDetection Example")
        self.set_border_width(__SPACING__)
        self.grid = Gtk.Grid(column_spacing=__SPACING__,
                             row_spacing=__SPACING__)
        self.add(self.grid)

        self.image_frame = Gtk.Frame(label='Real-time video', hexpand=True)
        self.image_frame.set_label_align(0.5, 0.5)
        self.image_frame.set_size_request(640, 480)

        self.event_frame = Gtk.Frame(label='Last alarm', hexpand=True)
        self.event_frame.set_label_align(0.5, 0.5)
        self.event_frame.set_size_request(640, 480)
        drawPoseLabel= Gtk.Label("Draw pose")
        self.draw_pose = Gtk.Switch()
        self.draw_pose.set_state(True)
        self.draw_pose.connect("state-set", self.on_draw_pose)
        self.reset_alarm = Gtk.Button(label="Reset alarm")
        self.reset_alarm.connect("clicked", self.on_reset_alarm)
        self.cmd_grid = Gtk.Grid(column_spacing=__SPACING__,
                             row_spacing=__SPACING__)
        self.cmd_grid.attach(drawPoseLabel, 0, 0, 1, 1)
        self.cmd_grid.attach(self.draw_pose, 1, 0, 1, 1)
        self.cmd_grid.attach(self.reset_alarm, 2, 0, 1, 1)
        self.grid.attach(self.cmd_grid, 0, 0, 1, 1)
        self.grid.attach(self.image_frame, 0, 1, 2, 3)
        self.grid.attach(self.event_frame, 0, 4, 2, 3)
        self.set_size_request(800, 600)

        self.gstreamer_frame_renderer = GstreamerRenderer(self.image_frame)
        self.gstreamer_event_renderer = GstreamerRenderer(self.event_frame)
        self.gstreamer_worker = GStreamerWorker(config, Analyzer(model=model),
                                                self.gstreamer_frame_renderer,
                                                on_event=self.on_alarm)
        self.gstreamer_worker.draw_pose = True
        self.gstreamer_worker.start()
        self.gstreamer_frame_renderer.start()
        self.gstreamer_event_renderer.start()

    def on_draw_pose(self, switch, state):
        self.gstreamer_worker.draw_pose = state

    def on_reset_alarm(self, button):
        self.event_frame.set_label("Last alarm")
        self.gstreamer_worker.alarm = False
        self.gstreamer_event_renderer.push_buffer(np.zeros((480, 640, 3)))

    def on_alarm(self, data):
        self.event_frame.set_label(f"Last alarm - {datetime.datetime.now().strftime('%d/%m/%Y %H:%M:%S')}")
        self.gstreamer_event_renderer.push_buffer(data)

    def stop_workers(self):
        if self.gstreamer_worker:
            self.gstreamer_worker.stop()
        if self.gstreamer_frame_renderer:
            self.gstreamer_frame_renderer.stop()
        if self.gstreamer_event_renderer:
            self.gstreamer_event_renderer.stop()
