from .gstreamer_base import *
gi.require_version('Gtk', '3.0')
gi.require_version('GdkX11', '3.0')
gi.require_version('GstVideo', '1.0')

# required
from gi.repository import GstVideo


class GstreamerRenderer(GstreamerBase):
    def __init__(self, frame):
        super().__init__()
        self.frame = frame
        self.area = None

    def get_pipeline(self):
        return "appsrc name=m_src ! videoconvert ! video/x-raw,format=BGRA ! gtksink name=m_sink"

    def push_buffer(self, mat):
        if self.src:
            if self.src.get_property('caps') is None:
                caps = Gst.Caps.from_string("video/x-raw,width={},height={},format=BGR".format(mat.shape[1], mat.shape[0]))
                self.src.set_property('caps', caps)
            buf = Gst.Buffer.new_wrapped(mat.tobytes())
            self.src.emit("push-buffer", buf)

    def start(self):
        self.pipeline = Gst.parse_launch(self.get_pipeline())
        self.src = self.pipeline.get_by_name('m_src')
        self.sink = self.pipeline.get_by_name('m_sink')

        bus = self.pipeline.get_bus()
        bus.add_signal_watch()
        bus.connect('message::eos', self.on_eos)
        bus.connect('message::error', self.on_error)
        bus.connect('message::warning', self.on_warning)

        self.area = self.sink.get_property("widget")
        self.area.set_hexpand(True)
        self.area.set_vexpand(True)
        self.frame.add(self.area)
        self.area.show()

        if not self.pipeline or not self.src or not self.sink:
            print("Not all elements could be created.")
        GstreamerBase.start(self)

    def stop(self):
        self.src.emit("end-of-stream")
        GstreamerBase.stop(self)

