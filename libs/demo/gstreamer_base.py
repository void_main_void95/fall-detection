import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GLib


class GstreamerBase:
    def __init__(self):
        self.pipeline = None
        self.src = None
        self.bus = None
        self.sink = None

    def start(self):
        ret = self.pipeline.set_state(Gst.State.PLAYING)
        if ret == Gst.StateChangeReturn.FAILURE:
            print("CameraWorker ------ Unable to set the pipeline to the playing state.")

    def stop(self):
        ret = self.pipeline.set_state(Gst.State.NULL)
        if ret == Gst.StateChangeReturn.FAILURE:
            print("CameraWorker ------ Unable to set the pipeline to the stop state.")

    def on_eos(self, bus, msg):
        print('on_eos()')

    def on_error(self, bus, msg):
        print('on_error():', msg.parse_error())

    def on_warning(self, bus, msg):
        print('on_error():', msg.parse_error())