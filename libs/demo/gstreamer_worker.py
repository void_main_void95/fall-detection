from .gstreamer_base import *
import numpy


class GStreamerWorker(GstreamerBase):
    def __init__(self, config, analyzer, renderer, on_event=None):
        super().__init__()
        self.config = config
        self.analyzer = analyzer
        self.renderer = renderer
        self.alarm = False
        self.draw_pose = False
        self.on_event = on_event

    @staticmethod
    def on_buffer(sink, self):
        sample = sink.emit("pull-sample")
        buf = sample.get_buffer()
        caps = sample.get_caps()
        success, map_info = buf.map(Gst.MapFlags.READ)
        if not success:
            raise RuntimeError("Could not map buffer data!")
        raw_data = map_info.data
        caps_structure = caps.get_structure(0)
        mat = numpy.ndarray(
            shape=(int(caps_structure.get_value('height')),
                   caps_structure.get_value('width'),
                   3),
            buffer=raw_data,
            dtype=numpy.uint8)
        buf.unmap(map_info)
        result, output_mat = self.analyzer.analyze(mat)
        if self.draw_pose:
            to_render = output_mat
        else:
            to_render = mat
        self.renderer.push_buffer(to_render)
        if result and not self.alarm:
            self.alarm = True
            if self.on_event:
                GLib.idle_add(self.on_event, to_render)
        return Gst.FlowReturn.OK

    def launch_pipeline(self, string_pipeline):
        self.pipeline = Gst.parse_launch(string_pipeline)
        self.sink = self.pipeline.get_by_name('m_sink')
        self.sink.set_property('max-buffers', 1)
        self.sink.set_property('drop', 'true')
        self.sink.set_property('emit-signals', True)
        self.src = self.pipeline.get_by_name('m_src')
        self.sink.connect("new-sample", self.on_buffer, self)
        bus = self.pipeline.get_bus()
        bus.add_signal_watch()
        bus.connect('message::eos', self.on_eos)
        bus.connect('message::error', self.on_error)
        bus.connect('message::warning', self.on_warning)

        if not self.pipeline or not self.src or not self.sink:
            print("Not all elements could be created.")

    def launch_video_pipeline(self):
        string_pipeline = 'filesrc name=m_src ! qtdemux ! queue ! avdec_h264 ! queue ! videoconvert ! video/x-raw,' \
                          'format=BGR ! videoscale ! video/x-raw,width=640,height=480 ! appsink name=m_sink'
        self.launch_pipeline(string_pipeline)
        self.src.set_property('location', self.config["video"])

    def launch_webcam_pipeline(self):
        string_pipeline = 'v4l2src name=m_src ! image/jpeg ! jpegdec ! videoconvert ! video/x-raw,format=BGR ! ' \
                          'videoscale ! video/x-raw,width=640,height=480 ! appsink name=m_sink'
        self.launch_pipeline(string_pipeline)
        self.src.set_property('device', self.config["webcam"])

    def start(self):
        if self.config["video"]:
            self.launch_video_pipeline()
        else:
            self.launch_webcam_pipeline()

        super().start()
