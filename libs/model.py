import torch.nn as nn
import torch


class FallDetector(nn.Module):
    def __init__(self, input_dim=None, hidden_dim=None, layer_dim=None,
                 output_dim=None, dropout_prob=None, state_dict=None):
        super(FallDetector, self).__init__()
        if state_dict is not None:
            self.input_dim = state_dict.pop("input_dim")
            self.hidden_dim = state_dict.pop("hidden_dim")
            self.layer_dim = state_dict.pop("layer_dim")
            self.output_dim = state_dict.pop("output_dim")
            self.dropout_prob = state_dict.pop("dropout_prob")
        elif input_dim is not None and hidden_dim is not None and layer_dim is not None and dropout_prob is not None:
            self.layer_dim = layer_dim
            self.hidden_dim = hidden_dim
            self.input_dim = input_dim
            self.output_dim = output_dim
            self.dropout_prob = dropout_prob
        else:
            raise RuntimeError

        self.gru = nn.GRU(
            self.input_dim, self.hidden_dim, self.layer_dim, batch_first=True, dropout=self.dropout_prob
        )
        self.fc = nn.Linear(self.hidden_dim, self.output_dim)
        self.softmax = nn.Softmax(dim=1)

        if state_dict is not None:
            super().load_state_dict(state_dict)

    def forward(self, x):
        h0 = torch.zeros(self.layer_dim, x.size(0), self.hidden_dim).to("cuda:0")
        h0.requires_grad_()
        h0 = h0.detach()
        out, _ = self.gru(x, h0)
        out = out[:, -1, :]
        out = self.fc(out)
        out = self.softmax(out)
        return out

    def predict(self, x):
        self.eval()
        with torch.no_grad():
            x = x.view([1, -1, self.input_dim])
            prediction = self.forward(x).argmax(1).to('cpu').item()
            return prediction
    
    def state_dict(self):
        state_dict = super().state_dict()
        state_dict["input_dim"] = self.input_dim
        state_dict["hidden_dim"] = self.hidden_dim
        state_dict["layer_dim"] = self.layer_dim
        state_dict["output_dim"] = self.output_dim
        state_dict["dropout_prob"] = self.dropout_prob
        return state_dict

    def load_state_dict(self, state_dict):
        state_dict.pop("input_dim")
        state_dict.pop("hidden_dim")
        state_dict.pop("layer_dim")
        state_dict.pop("output_dim")
        state_dict.pop("dropout_prob")
        super().load_state_dict(state_dict)
