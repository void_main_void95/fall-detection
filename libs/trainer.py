import torch
from matplotlib import pyplot as plt
import numpy
import os
from tqdm.auto import tqdm
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay


class FallDetectorTrainer:
    def __init__(self, model, loss_fn, optimizer, epochs=100, state_dict=None, model_label=None):
        self.model = model
        self.loss_fn = loss_fn
        self.optimizer = optimizer
        self.epochs = epochs
        self.model_label = model_label
        self.total_epochs = 0
        self.train_losses = []
        self.train_acc = []
        self.test_losses = []
        self.test_acc = []
        self.best_acc = 0
        self.train_cm = None
        self.test_cm = None
        self.net_checkpoint = None
        if state_dict:
            self.net_checkpoint = state_dict["net"]
            self.total_epochs = state_dict["epoch"]
            self.best_acc = state_dict["acc"]
            self.train_cm = state_dict["train_cm"]
            self.test_cm = state_dict["test_cm"]
            self.train_losses = state_dict["train_loss_data"]
            self.test_losses = state_dict["test_loss_data"]
            self.train_acc = state_dict["train_acc_data"]
            self.test_acc = state_dict["test_acc_data"]

    def _save_checkpoint(self):
        if self.model_label:
            model_name = f"FallDetection_model_{self.model_label}"
        else:
            model_name = "FallDetection_model"
        checkpoint = {
            "net": self.net_checkpoint,
            "acc": self.best_acc,
            "test_cm": self.test_cm,
            "train_cm": self.train_cm,
            "epoch": self.total_epochs,
            "train_loss_data": self.train_losses,
            "test_loss_data": self.test_losses,
            "train_acc_data": self.train_acc,
            "test_acc_data": self.test_acc
        }
        torch.save(checkpoint, os.path.join("models", model_name))

    def train_step(self, current_epoch, total_epochs, train_loader, batch_size=64):
        self.model.train()
        train_loss = []
        correct = 0
        total = 0
        train_loader_loop = tqdm(train_loader, position=0, leave=True)
        cm_y_pred = []
        cm_y = []
        for x_batch, y_batch in train_loader_loop:
            x_batch = x_batch.view([batch_size, -1, self.model.input_dim]).to("cuda:0")
            cm_y.extend(y_batch.long().numpy())
            y_batch = y_batch.to("cuda:0")
            self.optimizer.zero_grad()
            y_hat = self.model(x_batch)
            loss = self.loss_fn(y_hat, y_batch.long())
            loss.backward()
            self.optimizer.step()
            train_loss.append(loss.item())
            _, predicted = y_hat.max(1)
            cm_y_pred.extend(predicted.cpu().numpy())
            total += y_batch.size(0)
            correct += predicted.eq(y_batch).sum().item()
            train_loader_loop.set_description(f"Train step => epoch [{current_epoch}/{total_epochs}]")
            train_loader_loop.set_postfix(loss=numpy.mean(train_loss), acc=(100. * correct / total))
            train_loader_loop.update()
        train_loader_loop.close()
        cm = confusion_matrix(cm_y, cm_y_pred)
        return numpy.mean(train_loss), 100. * correct / total, cm

    def test_step(self, current_epoch, total_epochs, test_loader, batch_size=1):
        self.model.eval()
        test_loss = []
        correct = 0
        total = 0
        test_loader_loop = tqdm(test_loader, position=0, leave=True)
        cm_y_pred = []
        cm_y = []
        with torch.no_grad():
            for x_test, y_test in test_loader_loop:
                x_test = x_test.view([batch_size, -1, self.model.input_dim]).to("cuda:0")
                cm_y.extend(y_test.long().numpy())
                y_test = y_test.to("cuda:0")
                y_hat = self.model(x_test)
                loss = self.loss_fn(y_hat, y_test.long())
                test_loss.append(loss.item())
                _, predicted = y_hat.max(1)
                cm_y_pred.extend(predicted.cpu().numpy())
                total += y_test.size(0)
                correct += predicted.eq(y_test).sum().item()
                test_loader_loop.set_description(f"Test step => epoch [{current_epoch}/{total_epochs}]")
                test_loader_loop.set_postfix(loss=numpy.mean(test_loss), acc=(100. * correct / total))
                test_loader_loop.update()
        test_loader_loop.close()
        cm = confusion_matrix(cm_y, cm_y_pred)
        return numpy.mean(test_loss), 100. * correct / total, cm

    def train(self, train_loader, validation_loader, batch_size=64):
        current_total_epochs = self.total_epochs + self.epochs
        for epoch in range(self.total_epochs, current_total_epochs + 1):
            train_loss, train_acc, train_confusion_matrix = self.train_step(epoch, current_total_epochs, train_loader,
                                                                            batch_size)
            self.train_losses.append(train_loss)
            self.train_acc.append(train_acc)
            test_loss, test_acc, test_confusion_matrix = self.test_step(epoch, current_total_epochs, validation_loader,
                                                                        batch_size)
            self.test_losses.append(test_loss)
            self.test_acc.append(test_acc)
            self.total_epochs = epoch
            if test_acc >= self.best_acc:
                self.net_checkpoint = self.model.state_dict()
                self.best_acc = test_acc
                self.test_cm = test_confusion_matrix
                self.train_cm = train_confusion_matrix
                self._save_checkpoint()
        self._save_checkpoint()

    def test(self, test_loader, batch_size=1):
        loss, acc, confusion_matrix = self.test_step(0, 0, test_loader, batch_size)
        print(f"Acc: {acc:.1f}%")
        tp, fp, fn, tn = confusion_matrix.ravel()
        print(f"TP: {tp} FP: {fp} FN: {fn}, TN: {tn}")

    def plot_results(self):
        plt.figure(figsize=(18, 6))
        plt.subplot(1, 2, 1)
        plt.plot(self.train_losses)
        plt.plot(self.test_losses)
        plt.legend(['Training Loss', 'Test Loss'])
        plt.grid()
        plt.subplot(1, 2, 2)
        plt.plot(self.train_acc)
        plt.plot(self.test_acc)
        plt.legend(['Training Accuracy', 'Test Accuracy'])
        plt.grid()
        plt.suptitle('Results')
        result_file_name = "results.png"
        train_confusion_matrix_name = "train_confusion_matrix"
        test_confusion_matrix_name = "test_confusion_matrix"
        if self.model_label:
            result_file_name = f"results_{self.model_label}.png"
            train_confusion_matrix_name = f"train_confusion_matrix_{self.model_label}.png"
            test_confusion_matrix_name = f"test_confusion_matrix_{self.model_label}.png"
        plt.savefig(os.path.join("results", result_file_name))

        cm_image = ConfusionMatrixDisplay(confusion_matrix=self.train_cm)
        cm_image.plot()
        plt.savefig(os.path.join("results", train_confusion_matrix_name))

        cm_image = ConfusionMatrixDisplay(confusion_matrix=self.test_cm)
        cm_image.plot()
        plt.savefig(os.path.join("results", test_confusion_matrix_name))
