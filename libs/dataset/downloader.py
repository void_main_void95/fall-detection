from threading import Thread
from queue import Queue
import os
import zipfile
import urllib3
import io
import tarfile


class Downloader:
    def __init__(self, workdir):
        self.workdir = workdir
        self.max_workers = 4
        self.requests = Queue(self.max_workers)
        self.workers = []
        self.http_client = urllib3.PoolManager(self.max_workers)

    def get_ur_fall_requests(self):
        host = "62.93.43.105"
        for i in range(1, 31):
            filename = f"fall-{i:02}-cam0-rgb.zip"
            self.requests.put({
                "url": f"http://{host}/~mkepski/ds/data/{filename}",
                "folder": "URFallDetectionDataset",
                "filename": filename
            })

        self.requests.put({
            "url": f"http://{host}/~mkepski/ds/data/urfall-cam0-falls.csv",
            "folder": "URFallDetectionDataset",
            "filename": "urfall-cam0-falls.csv"
        })

        for i in range(1, 41):
            filename = f"adl-{i:02}-cam0-rgb.zip"
            self.requests.put({
                "url": f"http://{host}/~mkepski/ds/data/{filename}",
                "folder": "URFallDetectionDataset",
                "filename": filename
            })

        self.requests.put({
            "url": f"http://{host}/~mkepski/ds/data/urfall-cam0-adls.csv",
            "folder": "URFallDetectionDataset",
            "filename": "urfall-cam0-adls.csv"
        })

    def get_fall_dataset_requests(self):
        host = "falldataset.com"
        videos = ["1301", "1790", "722", "1378", "1392",
                  "807", "758", "1843", "569", "1260",
                  "489", "731", "1219", "1954", "581", "2123", "832", "786", "925"]
        for video in videos:
            self.requests.put({
                "url": f"https://{host}/data/{video}/{video}.tar.gz",
                "folder": "FallDetectionDataset",
                "filename": f"{video}.tar.gz"
            })
        broken_tar = "1176"
        self.requests.put({
            "url": f"https://{host}/data/{broken_tar}/{broken_tar}.tar.gz",
            "folder": f"FallDetectionDataset/{broken_tar}",
            "filename": f"{broken_tar}.tar.gz",
        })
        self.requests.put({
            "url": f"https://{host}/data/{broken_tar}/labels.csv",
            "folder": f"FallDetectionDataset/{broken_tar}",
            "filename": f"labels.csv",
        })

    def thread_routine(self):
        while True:
            request = self.requests.get()
            if request != "\n":
                print(f"Downloading {request['filename']}...")
                try:
                    target_folder = os.path.join(self.workdir, request["folder"])
                    if not os.path.isdir(target_folder):
                        os.makedirs(target_folder, exist_ok=True)
                    response = self.http_client.request("GET", request["url"])
                    if ".zip" in request["filename"]:
                        print(f"Decompressing {request['filename']}...")
                        file = zipfile.ZipFile(io.BytesIO(response.data))
                        file.extractall(target_folder)
                        file.close()
                    elif ".tar.gz" in request["filename"]:
                        print(f"Decompressing {request['filename']} ...")
                        file = tarfile.open(fileobj=io.BytesIO(response.data))
                        file.extractall(target_folder)
                        file.close()
                    else:
                        with open(os.path.join(target_folder, request["filename"]), "wb") as f:
                            f.write(response.data)
                            f.flush()
                    response.release_conn()
                except Exception as e:
                    print("Error! ", str(e))
                    break
            else:
                self.requests.put("\n")
                break

    def download_dataset(self):
        for i in range(self.max_workers):
            worker = Thread(target=self.thread_routine)
            self.workers.append(worker)
            worker.start()

        self.get_ur_fall_requests()
        self.get_fall_dataset_requests()
        self.close()

    def close(self):
        self.requests.put("\n")
        for worker in self.workers:
            worker.join()
        self.http_client.clear()
