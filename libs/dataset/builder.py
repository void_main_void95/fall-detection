import sys
sys.path.append('/usr/local/python')
import cv2
import numpy
import pandas
import os
from openpose import pyopenpose as op
from ..openpose_settings import openpose_settings


class FallDetectionDatasetBuilder:
    def __init__(self, no_pose=True):
        self.openpose = op.WrapperPython()
        self.openpose.configure(openpose_settings)
        self.running = False
        self.no_pose = no_pose

    def process_frame(self, mat):
        datum = op.Datum()
        datum.cvInputData = mat
        self.openpose.emplaceAndPop(op.VectorDatum([datum]))
        if datum.poseKeypoints is not None:
            pose = datum.poseKeypoints[0]
            pose = numpy.delete(pose, 2, 1)
            pose = pose.reshape(-1, 1)
            return pose, datum.cvOutputData
        return None

    def process_fall_dataset(self, path, output_path):
        fall_path = os.path.join(path, "FallDetectionDataset")

        for video in os.listdir(fall_path):
            print("Processing video {}".format(video))
            labels_file = os.path.join(fall_path, video, "labels.csv")
            frame_path = os.path.join(fall_path, video, "rgb")
            self.process_fall_dataset_video(frame_path, labels_file, os.path.join(output_path, video))

    def process_fall_dataset_video(self, video_path, label_file, output_path):
        if not os.path.isdir(output_path):
            os.mkdir(output_path)

        dataset = []
        dataset_file = os.path.join(output_path, "labels.csv")

        if os.path.isfile(dataset_file):
            print("Video already processed. Skipping.")
            return

        labels = pandas.read_csv(label_file)
        labels = labels[labels["class"] != 0]
        for index, row in labels.iterrows():
            frame_name = f"rgb_{row['index']:04}.png"
            frame_path = os.path.join(video_path, frame_name)
            if row["class"] != 0:
                mat = cv2.imread(frame_path)
                if mat is not None:
                    mat = cv2.resize(mat, (640, 480))
                    result = self.process_frame(mat)
                    if result:
                        label = 1
                        if row["class"] in [1, 2]:
                            label = 0
                        dataset.append((frame_name, result[0], label))
                        if not self.no_pose:
                            cv2.imwrite(os.path.join(output_path, frame_name), result[1])
        dataset = pandas.DataFrame(dataset, columns=['frame', 'pose', 'label'])
        dataset.to_csv(dataset_file, index=False)

    def build_all(self, path, output_path):
        if not self.running:
            self.openpose.start()
            self.running = True

        if not os.path.isdir(output_path):
            os.mkdir(output_path)

        self.process_ur_fall_dataset(path, output_path)
        self.process_fall_dataset(path, output_path)
        self.openpose.stop()
        self.running = False

    def process_ur_fall_dataset(self, path, output_path):
        ur_path = os.path.join(path, "URFallDetectionDataset")
        falls_csv = os.path.join(ur_path, "urfall-cam0-falls.csv")
        adls_csv = os.path.join(ur_path, "urfall-cam0-adls.csv")
        falls = []
        adls = []

        for item in os.listdir(ur_path):
            if ".csv" not in item:
                if "fall" in item:
                    falls.append(item)
                elif "adl" in item:
                    adls.append(item)

        self.process_ur_fall_dataset_dir(ur_path, falls, falls_csv, output_path)
        self.process_ur_fall_dataset_dir(ur_path, adls, adls_csv, output_path)

    def process_ur_fall_dataset_video(self, video_path, video_csv, output_path):
        if not os.path.isdir(output_path):
            os.mkdir(output_path)

        dataset = []
        dataset_file = os.path.join(output_path, "labels.csv")

        if os.path.isfile(dataset_file):
            print("Video already processed. Skipping.")
            return

        for index, row in video_csv.iterrows():
            frame_name = "{}-cam0-rgb-{}.png".format(row[0], f"{row[1]:03}")
            frame_path = os.path.join(video_path, frame_name)
            if row[2] != 0:
                mat = cv2.imread(frame_path)
                if mat is not None:
                    result = self.process_frame(mat)
                    if result:
                        if row[2] == -1:
                            dataset.append((frame_name, result[0], 0))
                        else:
                            dataset.append((frame_name, result[0], 1))
                        if not self.no_pose:
                            cv2.imwrite(os.path.join(output_path, frame_name), result[1])
        dataset = pandas.DataFrame(dataset, columns=['frame', 'pose', 'label'])
        dataset.to_csv(dataset_file, index=False)

    def process_ur_fall_dataset_dir(self, input_path, videos, csv_path, output_path):
        if not self.running:
            self.openpose.start()
            self.running = True

        input_csv = pandas.read_csv(csv_path, header=None)
        for video in videos:
            print("Processing video {}".format(video))
            video_base_name = video.split("-cam0-rgb")[0]
            video_csv = input_csv.loc[input_csv[0] == video_base_name]
            self.process_ur_fall_dataset_video(os.path.join(input_path, video), video_csv, os.path.join(output_path, video))
