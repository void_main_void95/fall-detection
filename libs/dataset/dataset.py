import numpy
import pandas
import re
import random
import ast
import os
import torch
from torch.utils.data import TensorDataset


def rp_normalization(image_width, image_height, record):
    x_c = image_width / 2
    y_c = image_height / 2
    x_dis = record[16] - x_c
    y_dis = record[17] - y_c
    for i in range(0, 15):
        record[i] = record[i] - x_dis
        record[i + 1] = record[i + 1] - y_dis
    return record


class FallDetectionDataset:
    def __init__(self, path=None):
        self.dataset = None
        self.path = path
        if path:
            self.load(path)

    @staticmethod
    def from_np_array(array_string):
        array_string = array_string.replace('\n ', ',')
        array_string = re.sub('\ +\]', ']', array_string)
        array_string = re.sub('\[\ +', '[', array_string)
        array_string = re.sub('\ +', ',', array_string)
        array = ast.literal_eval(array_string)
        nparray = numpy.array(array)
        return nparray

    def load(self, path):
        self.path = path
        self.dataset = {}

        for video in os.listdir(self.path):
            video_path = os.path.join(self.path, video)
            if os.path.isdir(video_path):
                self.dataset[video] = os.path.join(video_path, "labels.csv")

    @staticmethod
    def load_item(path):
        item = pandas.read_csv(path, converters={'pose': FallDetectionDataset.from_np_array})
        return item

    @staticmethod
    def save_item(item, path):
        item.to_csv(path, index=False)

    def get_dataset(self):
        dataset = pandas.DataFrame(columns=["frame", "pose", "label"])
        for video in self.dataset:
            labels = FallDetectionDataset.load_item(self.dataset[video])
            dataset = pandas.concat([dataset, labels], ignore_index=True)
        return dataset

    def _random_fill(self, rows, ur_videos, fall_videos, expected_size):
        while True:
            random_i = random.choice([0, 1, 2, 4])
            for i in range(4):
                if len(ur_videos) != 0:
                    ur_random = random.choice(ur_videos)
                    ur_videos.remove(ur_random)
                    labels = FallDetectionDataset.load_item(self.dataset[ur_random])
                    rows = pandas.concat([rows, labels], ignore_index=True)
                if random_i == i and len(fall_videos) != 0:
                    fall_random = random.choice(fall_videos)
                    fall_videos.remove(fall_random)
                    labels = FallDetectionDataset.load_item(self.dataset[fall_random])
                    rows = pandas.concat([rows, labels], ignore_index=True)
            if (expected_size - rows.shape[0] < 50) or (len(ur_videos) == 0 and len(fall_videos) == 0):
                return rows

    def train_test_split(self, train_to_validation=0.8, train_to_test_ratio=0.8, min_max=False,
                         reduce_features_use_rp=False, tensor_dataset=False):

        train_set_file = os.path.join(self.path, "train_set.csv")
        validation_set_file = os.path.join(self.path, "validation_set.csv")
        test_set_file = os.path.join(self.path, "test_set.csv")

        if os.path.isfile(train_set_file) and os.path.isfile(validation_set_file) and os.path.isfile(test_set_file):
            train_set = FallDetectionDataset.load_item(train_set_file)
            validation_set = FallDetectionDataset.load_item(validation_set_file)
            test_set = FallDetectionDataset.load_item(test_set_file)
        else:
            ur_videos = []
            fall_videos = []
            ur_size = 0
            fall_size = 0
            for video in self.dataset.keys():
                labels = FallDetectionDataset.load_item(self.dataset[video])
                if "adl" in video or "fall" in video:
                    ur_videos.append(video)
                    ur_size += labels.shape[0]
                else:
                    fall_videos.append(video)
                    fall_size += labels.shape[0]

            #check the size
            total_size = ur_size + fall_size
            train_total = round(total_size * train_to_test_ratio)
            test_set_size = total_size - train_total
            train_set_size = round(train_total * train_to_validation)
            validation_set_size = train_total - train_set_size

            train_set = pandas.DataFrame(columns=["frame", "pose", "label"])
            validation_set = pandas.DataFrame(columns=["frame", "pose", "label"])
            test_set = pandas.DataFrame(columns=["frame", "pose", "label"])

            train_set = self._random_fill(train_set, ur_videos, fall_videos, train_set_size)
            validation_set = self._random_fill(validation_set, ur_videos, fall_videos, validation_set_size)
            test_set = self._random_fill(test_set, ur_videos, fall_videos, test_set_size)

            FallDetectionDataset.save_item(train_set, train_set_file)
            FallDetectionDataset.save_item(validation_set, validation_set_file)
            FallDetectionDataset.save_item(test_set, test_set_file)

        if min_max or reduce_features_use_rp:
            train_set = self._pre_process(train_set, min_max, reduce_features_use_rp)
            validation_set = self._pre_process(validation_set, min_max, reduce_features_use_rp)
            test_set = self._pre_process(test_set, min_max, reduce_features_use_rp)

        if tensor_dataset:
            train_set = self._to_torch_dataset(train_set)
            validation_set = self._to_torch_dataset(validation_set)
            test_set = self._to_torch_dataset(test_set)

        return train_set, validation_set, test_set

    @staticmethod
    def _pre_process(video_rows, min_max=False, reduce_features_use_rp=False):
        tmp_video_rows = video_rows.copy(deep=True)
        if reduce_features_use_rp:
            tmp_video_rows["pose"] = tmp_video_rows["pose"].map(lambda row: row[:30])
            tmp_video_rows["pose"] = tmp_video_rows["pose"].map(lambda row: rp_normalization(640, 480, row))
        if min_max:
            val_max = tmp_video_rows["pose"].map(lambda x: x.max()).max()
            val_min = tmp_video_rows["pose"].map(lambda x: x.min()).min()
            tmp_video_rows["pose"] = tmp_video_rows["pose"].map(lambda x: (x - val_min) / (val_max - val_min))
        return tmp_video_rows

    @staticmethod
    def _to_torch_dataset(dataset):
        features = torch.Tensor(numpy.stack(dataset["pose"], axis=0))
        labels = torch.Tensor(dataset["label"])
        return TensorDataset(features, labels)
