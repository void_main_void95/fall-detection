import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk

__SPACING__ = 10


class Keyboard(Gtk.Frame):
    def __init__(self):
        super(Keyboard, self).__init__(label="Keyboard")
        self.set_label_align(0.5, 0.5)
        self.grid = Gtk.Grid(column_spacing=__SPACING__,
                             row_spacing=__SPACING__,)
        self.grid.set_border_width(__SPACING__)
        self.labels_group = Gtk.ShortcutsGroup(title="Labels", hexpand=True)
        self.commands_group = Gtk.ShortcutsGroup(title="Commands", hexpand=True)
        self.add(self.grid)
        self.grid.attach(self.labels_group, 0, 0, 1, 2)
        self.grid.attach(self.commands_group, 0, 2, 1, 2)
        labels = self.get_labels()
        for label in labels:
            self.labels_group.pack_start(label, True, True, 0)
        cmds = self.get_commands()
        for cmd in cmds:
            self.commands_group.pack_start(cmd, True, True, 0)

    @staticmethod
    def get_labels():
        labels = ["no_fall : 0", "fall : 1"]
        keys = ["A", "S"]

        labels_obj = []
        for idx, item in enumerate(labels):
            labels_obj.append(Gtk.ShortcutsShortcut(accelerator=keys[idx], title=item))
        return labels_obj

    @staticmethod
    def get_commands():
        cmds = ["next frame", "prev frame"]
        keys = ["Right", "Left"]
        cmds_obj = []

        for idx, item in enumerate(cmds):
            cmds_obj.append(Gtk.ShortcutsShortcut(accelerator=keys[idx], title=item))
        return cmds_obj
