import gi
import os

from libs.dataset import FallDetectionDataset
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GdkPixbuf, Gdk, GLib
from .keyboard import Keyboard
import cv2

__SPACING__ = 10


class FallDetectionDatasetLabeler(Gtk.Window):
    def __init__(self):
        super().__init__(title="FallDetection Dataset Labeler")
        self.set_border_width(__SPACING__)
        self.grid = Gtk.Grid(column_spacing=__SPACING__,
                             row_spacing=__SPACING__)
        self.add(self.grid)

        choose_button = Gtk.Button(label="Choose Folder", hexpand=True)
        save_button = Gtk.Button(label="Save", hexpand=True)
        save_and_close_button = Gtk.Button(label="Save and close", hexpand=True)
        choose_button.connect("clicked", self.on_folder_clicked)
        save_button.connect("clicked", self.on_save)
        save_and_close_button.connect("clicked", self.on_save_and_close)
        self.buttons_grid = Gtk.Grid(column_spacing=__SPACING__,
                                     row_spacing=__SPACING__)
        self.buttons_grid.attach(choose_button, 0, 0, 1, 1)
        self.buttons_grid.attach(save_button, 0, 1, 1, 1)
        self.buttons_grid.attach(save_and_close_button, 0, 2, 1, 1)

        self.image_frame = Gtk.Frame(label='Frame', hexpand=True)
        self.image_frame.set_label_align(0.5, 0.5)
        self.image_frame.set_size_request(640, 480)
        self.img = Gtk.Image()
        self.image_frame.add(self.img)

        self.keyboard_frame = Keyboard()

        self.current_labels_frame = Gtk.Frame(label='Current Label')
        self.current_labels_frame.set_label_align(0.5, 0.5)
        self.current_label = Gtk.Label(label="")
        self.current_labels_frame.add(self.current_label)

        self.grid.attach(self.current_labels_frame, 0, 0, 1, 1)
        self.grid.attach(self.keyboard_frame, 0, 1, 1, 1)
        self.grid.attach(self.buttons_grid, 0, 2, 1, 1)
        self.grid.attach(self.image_frame, 1, 0, 2, 3)

        self.set_size_request(800, 600)
        self.connect("key-press-event", self.on_key_press_event)
        self.currentFrame = 0
        self.dataset = None
        self.workdir = None
        self.img_buffer = None

    def load_image(self):
        if len(self.dataset) > 0:
            filename = os.path.join(self.workdir, self.dataset.loc[self.currentFrame, "frame"])
            image = cv2.imread(filename)
            height, width, channels = image.shape
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            image = cv2.resize(image, (width, height)).ravel()
            image_pix_buf = GdkPixbuf.Pixbuf.new_from_data(image, GdkPixbuf.Colorspace.RGB, False, 8, width, height,
                                                           3 * width)
            image_pix_buf = image_pix_buf.scale_simple(640, 480, GdkPixbuf.InterpType.BILINEAR)
            self.img.clear()
            self.img.set_from_pixbuf(image_pix_buf)
            self.update_frame_label()

    def next_item(self):
        if (self.currentFrame + 1) < len(self.dataset):
            self.currentFrame += 1
            self.load_image()
            self.set_current_label()

    def prev_item(self):
        if (self.currentFrame - 1) >= 0:
            self.currentFrame -= 1
            self.load_image()
            self.set_current_label()

    def set_current_label(self):
        if len(self.dataset) > 0:
            label = self.dataset.loc[self.currentFrame, "label"]
            self.current_label.set_label(f"{label}")

    def update_frame_label(self):
        filename = self.dataset.loc[self.currentFrame, "frame"]
        n_frame = filename.split('.')[0].split('-')[-1]
        self.image_frame.set_label("Frame {}".format(n_frame))

    def update_current_label(self, label):
        self.dataset.loc[self.currentFrame, "label"] = label
        self.set_current_label()

    def on_folder_clicked(self, widget):
        dialog = Gtk.FileChooserDialog(
            title="Please choose a folder",
            parent=self,
            action=Gtk.FileChooserAction.SELECT_FOLDER,
        )
        dialog.add_buttons(
            Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
            "Select", Gtk.ResponseType.OK
        )
        dialog.set_default_size(800, 400)

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            self.workdir = dialog.get_filename()
            if self.load_dataset():
                self.currentFrame = 0
                self.load_image()
                self.set_current_label()
                self.set_title("FallDetection Dataset Labeler {}".format(self.workdir))

        elif response == Gtk.ResponseType.CANCEL:
            print("Cancel clicked")

        dialog.destroy()

    def load_dataset(self):
        if self.workdir is not None:
            dataset_file_path = os.path.join(self.workdir, "labels.csv")
            if os.path.isfile(dataset_file_path):
                self.dataset = FallDetectionDataset.load_item(dataset_file_path)
                return True
        return False

    def on_key_press_event(self, widget, event):
        if self.dataset is None:
            return
        if event.keyval == 65363:
            self.next_item()
        elif event.keyval == 65361:
            self.prev_item()
        elif event.keyval == 97 or event.keyval == 65:
            self.update_current_label(0)
        elif event.keyval == 115 or event.keyval == 83:
            self.update_current_label(1)

    def on_save(self, event):
        if self.dataset is not None:
            FallDetectionDataset.save_item(self.dataset, os.path.join(self.workdir, "labels.csv"))

    def on_save_and_close(self, event):
        if self.dataset is not None:
            FallDetectionDataset.save_item(self.dataset, os.path.join(self.workdir, "labels.csv"))
            self.current_label.set_label("")
            self.image_frame.set_label("Frame")
            self.dataset = None
            self.img.clear()
            self.set_title("FallDetection Dataset Labeler")


