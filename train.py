from libs.dataset import *
from libs.trainer import *
from libs.model import *
from libs.seed import seed
import random
import torch
import torch.optim as optim
from torch.utils.data import TensorDataset, DataLoader
from argparse import ArgumentParser

random.seed(seed)
torch.manual_seed(seed)

if __name__ == "__main__":
    parser = ArgumentParser(description='train fall-detector')
    parser.add_argument('--lr', help="set learning rate (Default 0.0001)", type=float,
                        default=0.0001)
    parser.add_argument('--wd', help="set weights decay (Default 1e-6)", type=float,
                        default=1e-6)
    parser.add_argument('--bs', help="set batch size  (Default 916)", type=int,
                        default=916)
    parser.add_argument('--hd', help="set hidden dim  (Default 512)", type=int,
                        default=512)
    parser.add_argument('--dataset', help="dataset root directory", type=str, required=True)
    parser.add_argument('--model_label', help="extra label to append to model checkpoint file", type=str,
                        default=None)
    parser.add_argument('--resume', help="resume training from checkpoint",  action='store_true')
    parser.add_argument('--use_min_max', help="use min-max normalization",  action='store_true')
    parser.add_argument('--reduce_features_use_rp', help="reduce 25 pose joints to 15 and apply RP Normalization",
                        action='store_true')
    parser.add_argument('--mode', help='network mode', type=str,
                        choices=['train', 'test'], default='train')

    args = parser.parse_args()
    dataset = FallDetectionDataset(args.dataset)

    if args.use_min_max and args.reduce_features_use_rp:
        parser.error("You must use only one flag between --use_min_max and --reduce_features_use_rp")
        exit(1)

    if args.resume or args.mode == "test":
        if args.model_label:
            checkpoint = f"models/FallDetection_model_{args.model_label}"
        else:
            checkpoint = "models/FallDetection_model"
        state_dict = torch.load(checkpoint)
        model = FallDetector(state_dict=state_dict["net"])
    else:
        checkpoint = None
        state_dict = None
        settings = {
            "input_dim": 30 if args.reduce_features_use_rp else 50,
            "output_dim": 2,
            "hidden_dim": args.hd,
            "layer_dim": 1,
            "dropout_prob": 0
        }
        model = FallDetector(**settings)

    print("Loading dataset...")
    trainSet, validationSet, testSet = dataset.train_test_split(train_to_test_ratio=0.8, train_to_validation=0.8,
                                                                min_max=args.use_min_max,
                                                                reduce_features_use_rp=args.reduce_features_use_rp,
                                                                tensor_dataset=True)
    loss_fn = nn.CrossEntropyLoss()
    optimizer = optim.Adam(model.parameters(), lr=args.lr, weight_decay=args.wd)

    model.to("cuda:0")

    trainer = FallDetectorTrainer(model=model, loss_fn=loss_fn, optimizer=optimizer, epochs=500,
                                  state_dict=state_dict, model_label=args.model_label)
    if args.mode == "train":
        train_loader = DataLoader(trainSet, batch_size=args.bs, shuffle=False, drop_last=True)
        validation_loader = DataLoader(validationSet, batch_size=args.bs, shuffle=False, drop_last=True)
        trainer.train(train_loader, validation_loader, batch_size=args.bs)
        trainer.plot_results()
    else:
        test_loader = DataLoader(testSet, batch_size=args.bs, shuffle=False, drop_last=True)
        trainer.test(test_loader, batch_size=args.bs)

    print("Complete!")

