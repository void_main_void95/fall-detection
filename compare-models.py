import torch
import os
from matplotlib import pyplot as plt
models_labels = ["no_normalization", "remove_falling", "min_max", "remove_falling_use_rp_reduce_features"]
from argparse import ArgumentParser


def plot_vs(data, labels, filename):
    plt.figure(figsize=(18, 6))
    for item in data:
        plt.plot(item)
    plt.legend(labels)
    plt.grid()
    plt.savefig(filename)


if __name__ == "__main__":
    parser = ArgumentParser(description='compare different models')
    parser.add_argument('--models_dir', help="models folder", type=str,
                        default="models", required=True)
    parser.add_argument('--results_dir', help="results folder", type=str,
                        default="results")
    parser.add_argument('--filter', help="use only models that match this filter", type=str)
    parser.add_argument('--label', help="extra label to append to compare graphs", type=str)
    args = parser.parse_args()
    train_acc_values = []
    test_acc_values = []
    train_loss_values = []
    test_loss_values = []
    labels = []
    indicators = []

    for model in os.listdir(args.models_dir):
        label = model.replace("FallDetection_model_", "")
        if args.filter is not None:
            if args.filter not in label:
                continue
        data = torch.load(f"models/FallDetection_model_{label}")
        labels.append(label)
        train_acc_values.append(data["train_acc_data"])
        train_loss_values.append(data["train_loss_data"])

        test_acc_values.append(data["test_acc_data"])
        test_loss_values.append(data["test_loss_data"])
        tn, fp, fn, tp = data["test_cm"].ravel()
        recall = 100. * tp / (tp + fn)
        precision = 100. * tp / (tp + fp)
        specificity = 100. * tn / (tn + fp)
        indicators_string = f"Model: {label} => acc: {data['acc']:.1f}%, recall: {recall:.1f}%, " \
                            f"precision: {precision:.1f}%, specificity: {specificity:.1f}%"

        indicators.append(indicators_string)
        print(indicators_string)

    train_acc_file = "train_acc_vs.png"
    train_loss_file = "train_loss_vs.png"
    test_acc_file = "test_acc_vs.png"
    test_loss_file = "test_loss_vs.png"
    indicators_file = "indicators.txt"
    if args.label:
        train_acc_file = f"{args.label}_{train_acc_file}"
        train_loss_file = f"{args.label}_{train_loss_file}"
        test_acc_file = f"{args.label}_{test_acc_file}"
        test_loss_file = f"{args.label}_{test_loss_file}"
        indicators_file = f"{args.label}_{indicators_file}"

    fp = open(os.path.join(args.results_dir, indicators_file), "w")
    for line in indicators:
        fp.write(line + "\n")
        fp.flush()
    fp.close()
    plot_vs(train_acc_values, labels,  os.path.join(args.results_dir, train_acc_file))
    plot_vs(test_acc_values, labels, os.path.join(args.results_dir, test_acc_file))
    plot_vs(train_loss_values, labels,  os.path.join(args.results_dir, train_loss_file))
    plot_vs(test_loss_values, labels, os.path.join(args.results_dir, test_loss_file))
