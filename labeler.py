import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from libs.labeler.main_window import FallDetectionDatasetLabeler

if __name__ == "__main__":
    win = FallDetectionDatasetLabeler()
    win.connect("destroy", Gtk.main_quit)
    win.show_all()
    Gtk.main()