import os.path

from libs.dataset import *
from argparse import ArgumentParser

if __name__ == "__main__":
    parser = ArgumentParser(description='build dataset')
    parser.add_argument('--input', help="root directory for original datasets", required=True)
    parser.add_argument('--output', help="root directory for processed dataset", required=True)
    parser.add_argument('--no_pose', help="disable frame saving", action='store_true', default=False)

    args = parser.parse_args()

    if args.input:
        if not os.path.isdir(args.input):
            os.mkdir(args.input)
        if len(os.listdir(args.input)) == 0:
            print("Downloading datasets...")
            downloader = Downloader(args.input)
            downloader.download_dataset()

        builder = FallDetectionDatasetBuilder(args.no_pose)
        builder.build_all(args.input, args.output)
