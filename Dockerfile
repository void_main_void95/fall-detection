FROM nvcr.io/nvidia/pytorch:22.08-py3 

ENV DEBIAN_FRONTEND "noninteractive"

RUN apt-get update && apt-get install -y  \
        libatlas-base-dev \
        libprotobuf-dev \
        libleveldb-dev \
        libsnappy-dev \
        libhdf5-serial-dev \
        protobuf-compiler \
        libboost-all-dev \
        libgflags-dev \
        libgoogle-glog-dev \
        liblmdb-dev \
        libssl-dev \
        pciutils \
        ocl-icd-opencl-dev \
        libviennacl-dev \
        libswscale-dev \
        libtbb2 \
        libtbb-dev \
        libjpeg-dev \
        libpng-dev \
        libtiff-dev \
        libavformat-dev \
        libpq-dev \
        libhdf5-dev \
        nlohmann-json3-dev \
        wget \
        libopencv-dev \
        python3-opencv \
        libgstreamer1.0-dev \
        libgstreamer-plugins-base1.0-dev \
        libgstreamer-plugins-bad1.0-dev \
        gstreamer1.0-plugins-base \
        gstreamer1.0-plugins-good \
        gstreamer1.0-plugins-bad \
        gstreamer1.0-plugins-ugly \
        gstreamer1.0-libav \
        gstreamer1.0-doc \
        gstreamer1.0-tools \
        gstreamer1.0-x \
        gstreamer1.0-alsa \
        gstreamer1.0-gl \
        gstreamer1.0-gtk3 \
        gstreamer1.0-qt5 \
        gstreamer1.0-pulseaudio \
        hwloc-nox \
        libhwloc-dev \
        libhwloc-plugins \
        libcairo2-dev \
        libxt-dev \
        libgirepository1.0-dev \
        libgtk-3-dev \
        dbus-x11 \
        libcanberra-gtk-module \
        libcanberra-gtk3-module \
        packagekit-gtk3-module

WORKDIR /opt

RUN echo "Downloading and building OpenPose..." && \
        git clone https://github.com/CMU-Perceptual-Computing-Lab/openpose.git && \
        cd openpose && \
        git checkout v1.7.0 && \
        mkdir -p build && \
        cd build && \
        cmake -DCUDA_cublas_device_LIBRARY="/usr/lib/x86_64-linux-gnu" -DBUILD_PYTHON=1 -DUSE_CUDNN=0 .. && \
        sed -ie 's/set(TURING "75")/#&/g'  ../cmake/Cuda.cmake && \
        sed -ie 's/set(TURING "75")/#&/g'  ../3rdparty/caffe/cmake/Cuda.cmake && \
        make -j$(nproc) && \
        wget -P ../models/pose/coco/ https://github.com/foss-for-synopsys-dwc-arc-processors/synopsys-caffe-models/raw/master/caffe_models/openpose/caffe_model/pose_iter_440000.caffemodel && \
        make install

WORKDIR /workdir

RUN groupadd -r user -g 1000 && \
    useradd -m --uid 1000 -r -g user -G audio,video user && \
    yes password | passwd user

RUN python3 -m pip install pycairo PyGObject

ENV PATH="${PATH}:/opt/hpcx/ompi/bin"
ENV LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:/opt/hpcx/ompi/lib"

#USER user

CMD [ "/bin/bash" ]

