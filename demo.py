from argparse import ArgumentParser
import gi
gi.require_version('Gst', '1.0')
gi.require_version('Gtk', '3.0')
gi.require_version('GdkX11', '3.0')
gi.require_version('GstVideo', '1.0')
from gi.repository import Gtk, Gst, GLib

from libs.demo import MainWindow
import signal

if __name__ == "__main__":
    parser = ArgumentParser(description='fall-detector demo')
    parser.add_argument('--model', help="Model to use", type=str, required=True)
    parser.add_argument('--video', help="path to a mp4 video file", type=str)
    parser.add_argument('--webcam', help="path to a usb camera", type=str)
    args = parser.parse_args()
    config = {
        "video": None,
    }
    if args.video:
        config["video"] = args.video
    elif args.webcam:
        config["webcam"] = args.webcam
    else:
        parser.print_usage()
        exit(1)

    Gst.init(None)
    win = MainWindow(config, args.model)

    win.show_all()

    def close(param):
        win.stop_workers()
        Gtk.main_quit()

    win.connect("destroy", close)
    GLib.unix_signal_add(GLib.PRIORITY_DEFAULT, signal.SIGINT, close, None)
    Gtk.main()


